[![VENDOR](https://img.shields.io/badge/vendor-teufels-blue.svg)](https://bitbucket.org/teufels/workspace/projects/TTER)
[![PACKAGE](https://img.shields.io/badge/package-tt3--barometer-orange.svg)](https://bitbucket.org/teufels/tt3-barometer/src/main/)
[![KEY](https://img.shields.io/badge/extension--key-tt3__barometer-red.svg)](https://bitbucket.org/teufels/tt3-barometer/src/main/)
![version](https://img.shields.io/badge/version-1.0.*-yellow.svg?style=flat-square)

[ ṯeufels ] Barometer
==========
Create & List Barometer(s)

#### This version supports TYPO3
![CUSTOMER](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req teufels/tt3-barometer`

***

### Requirements
- `jQuery`

***

### How to use
- Install with composer
- Import Static Template (before sitepackage)
- Create "Barometer" Records in Backend 
- place Plugin and select "Barometer(s)" to display
- select "Animation" 
  - On "Custom" you could create own Animation in your sitepackage by 
    override tt3_barometer.js in sitepackage (TS setup & js file) and create custom animation

***

### Update & Migration from hive_barometer
1. in composer.json replace `beewilly/hive_barometer` with `"teufels/tt3-barometer":"^1.0"`
2. Composer update
3. Include TypoScript set `[teufels] Barometer`
4. Analyze Database Structure -> Add tables & fields (do not remove old hive_barometer yet)
5. Perform Upgrade Wizards `[teufels] Barometer`
6. Analyze Database Structure -> Remove tables & unused fields (remove old hive_barometer now)
7. class & id changed -> adjust styling in sitepackage (e.g. tx-hive-barometer => tx-tt3-barometer)
8. check & adjust be user group access rights

***

### Changelog
- 1.0.2 fix renderContentElementPreviewFromFluidTemplate changed parameters
- 1.0.1 changed deprecated allowTableOnStandardPages() to ignorePageTypeRestriction (https://docs.typo3.org/m/typo3/reference-tca/12.4/en-us/Ctrl/Properties/Security.html#ctrl-security-ignorepagetyperestriction)
- 1.0.0 intial from [hive_barometer](https://bitbucket.org/teufels/hive_barometer/src/)