<?php
defined('TYPO3') || die();

$extensionKey = 'tt3_barometer';
$extensionTitle = '[ṯeufels] Barometer';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extensionKey, 'Configuration/TypoScript', $extensionTitle);
