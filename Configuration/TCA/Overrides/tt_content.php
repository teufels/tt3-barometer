<?php
defined('TYPO3') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Tt3Barometer',
    'Barometerlist',
    '[ṯeufels] Barometer',
    'EXT:tt3_barometer/Resources/Public/Icons/Plugin.svg',
    'teufels'
);

$pluginSignature = 'tt3barometer_barometerlist';
$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$pluginSignature] = 'tt3barometer_plugin_icon';
$GLOBALS['TCA']['tt_content']['types']['list']['previewRenderer'][$pluginSignature] = \Teufels\Tt3Barometer\Preview\PreviewRenderer::class;
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:tt3_barometer/Configuration/FlexForms/flexform_list.xml'
);