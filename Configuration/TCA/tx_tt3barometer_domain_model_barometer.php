<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer',
        'label' => 'backend_title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backend_title,title,description',
        'iconfile' => 'EXT:tt3_barometer/Resources/Public/Icons/Element.svg',
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
    ],
    'types' => [
        '1' => ['showitem' => 'backend_title, title, description, value_min, value_max, value_now, unit, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language, sys_language_uid, l10n_parent, l10n_diffsource, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_tt3barometer_domain_model_barometer',
                'foreign_table_where' => 'AND {#tx_tt3barometer_domain_model_barometer}.{#pid}=###CURRENT_PID### AND {#tx_tt3barometer_domain_model_barometer}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'backend_title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.backend_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required',
                'default' => ''
            ],
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            
        ],
        'value_min' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.value_min',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2,required',
                'default' => '0'
            ]
        ],
        'value_max' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.value_max',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2,required',
            ]
        ],
        'value_now' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.value_now',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
        'unit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:tt3_barometer/Resources/Private/Language/locallang_db.xlf:tx_tt3barometer_domain_model_barometer.unit',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => ''
            ],
        ],
    
    ],
];
