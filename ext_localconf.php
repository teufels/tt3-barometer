<?php
defined('TYPO3') || die();

(static function() {

    /**
     * Load PageTS
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
        @import \'EXT:tt3_barometer/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig\'
        @import \'EXT:tt3_barometer/Configuration/TsConfig/Page/BackendPreview.tsconfig\'
    ');

    /**
     * register Icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'tt3barometer_plugin_icon',
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => 'EXT:tt3_barometer/Resources/Public/Icons/Plugin.svg',
        ]
    );

    /**
     * configure Plugin
     */
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Tt3Barometer',
        'Barometerlist',
        [
            \Teufels\Tt3Barometer\Controller\BarometerController::class => 'list'
        ],
        // non-cacheable actions
        [
            \Teufels\Tt3Barometer\Controller\BarometerController::class => 'list'
        ]
    );

})();



