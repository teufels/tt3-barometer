CREATE TABLE tx_tt3barometer_domain_model_barometer (
	backend_title varchar(255) NOT NULL DEFAULT '',
	title varchar(255) NOT NULL DEFAULT '',
	description text,
	value_min double(11,2) NOT NULL DEFAULT '0.00',
	value_max double(11,2) NOT NULL DEFAULT '0.00',
	value_now varchar(255) NOT NULL DEFAULT '',
	unit varchar(255) NOT NULL DEFAULT '',
);