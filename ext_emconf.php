<?php

$EM_CONF[$_EXTKEY] = [
    'title' => '[teufels] Barometer',
    'description' => 'Create & List Barometer(s)',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teufels.com',
    'author_company' => 'teufels GmbH',
    'state' => 'stable',
    'version' => '1.0.2',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-0.0.0',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
