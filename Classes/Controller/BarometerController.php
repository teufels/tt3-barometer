<?php

declare(strict_types=1);

namespace Teufels\Tt3Barometer\Controller;


/**
 * This file is part of the "tt3_barometer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 teufels GmbH <digital@teufels.com>
 */

/**
 * BarometerController
 */
class BarometerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * barometerRepository
     *
     * @var \Teufels\Tt3Barometer\Domain\Repository\BarometerRepository
     */
    protected $barometerRepository = null;

    /**
     * @param \Teufels\Tt3Barometer\Domain\Repository\BarometerRepository $barometerRepository
     */
    public function injectBarometerRepository(\Teufels\Tt3Barometer\Domain\Repository\BarometerRepository $barometerRepository)
    {
        $this->barometerRepository = $barometerRepository;
    }

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $singleRecords = (string)$this->settings['singleRecords'];
        $barometers = $this->barometerRepository->findByUids($singleRecords);
        $this->view->assign('barometers', $barometers);
        return $this->htmlResponse();
    }


}
