<?php

declare(strict_types=1);

/*
 * This file is part of the "tt3_barometer" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

namespace Teufels\Tt3Barometer\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Attribute\UpgradeWizard;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

#[UpgradeWizard('tt3barometerDataUpdater')]
class DataUpdater implements UpgradeWizardInterface
{

    public function getTitle(): string
    {
        return '[teufels] Barometer: Migrate data';
    }

    public function getDescription(): string
    {
        $description = 'This update wizard migrates all data from tx_hivebarometer_domain_model_barometer to the new tx_tt3barometer_domain_model_barometer';
        if($this->checkMigrationTableExist()) { $description .= ': ' . count($this->getMigrationRecords()); }
        return $description;
    }

    public function getPrerequisites(): array
    {
        return [
            DatabaseUpdatedPrerequisite::class,
        ];
    }

    public function updateNecessary(): bool
    {
        return $this->checkIfWizardIsRequired();
    }

    public function executeUpdate(): bool
    {
        return $this->performMigration();
    }

    public function checkIfWizardIsRequired(): bool
    {
        return $this->checkMigrationTableExist();
    }

    public function performMigration(): bool
    {
        //data
        $sql = "INSERT INTO tx_tt3barometer_domain_model_barometer(uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,sys_language_uid,l10n_parent,backend_title,title,description,value_min,value_max,value_now,unit) 
        SELECT uid,pid,tstamp,crdate,deleted,hidden,starttime,endtime,sys_language_uid,l10n_parent,backend_title,title,description,value_min,value_max,value_now,unit
        FROM tx_hivebarometer_domain_model_barometer WHERE deleted = 0";

        /** @var Connection $connection */
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_tt3barometer_domain_model_barometer');
        /** @var DriverStatement $statement */
        $statement = $connection->prepare($sql);
        $statement->execute();

        return true;
    }

    protected function getMigrationRecords(): array
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_hivebarometer_domain_model_barometer');
        $queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        return $queryBuilder
            ->select('uid')
            ->from('tx_hivebarometer_domain_model_barometer')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function checkMigrationTableExist(): bool {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_hivebarometer_domain_model_barometer')
            ->getSchemaManager()
            ->tablesExist(['tx_hivebarometer_domain_model_barometer']);
    }

}
