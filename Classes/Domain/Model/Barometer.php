<?php

declare(strict_types=1);

namespace Teufels\Tt3Barometer\Domain\Model;


/**
 * This file is part of the "tt3_barometerr" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 teufels GmbH <digital@teufels.com>
 */

/**
 * Barometer
 */
class Barometer extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * backendTitle
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $backendTitle = '';

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * start value
     *
     * @var float
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $valueMin = 0.0;

    /**
     * end/target value
     *
     * @var float
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $valueMax = 0.0;

    /**
     * current value
     *
     * @var string
     */
    protected $valueNow = '';

    /**
     * unit
     *
     * @var string
     */
    protected $unit = '';

    /**
     * Returns the backendTitle
     *
     * @return string
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle(string $backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * Returns the valueMin
     *
     * @return float
     */
    public function getValueMin()
    {
        return $this->valueMin;
    }

    /**
     * Sets the valueMin
     *
     * @param float $valueMin
     * @return void
     */
    public function setValueMin(float $valueMin)
    {
        $this->valueMin = $valueMin;
    }

    /**
     * Returns the valueMax
     *
     * @return float
     */
    public function getValueMax()
    {
        return $this->valueMax;
    }

    /**
     * Sets the valueMax
     *
     * @param float $valueMax
     * @return void
     */
    public function setValueMax(float $valueMax)
    {
        $this->valueMax = $valueMax;
    }

    /**
     * Returns the valueNow
     *
     * @return string
     */
    public function getValueNow()
    {
        return $this->valueNow;
    }

    /**
     * Sets the valueNow
     *
     * @param string $valueNow
     * @return void
     */
    public function setValueNow(string $valueNow)
    {
        $this->valueNow = $valueNow;
    }

    /**
     * Returns the unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Sets the unit
     *
     * @param string $unit
     * @return void
     */
    public function setUnit(string $unit)
    {
        $this->title = $unit;
    }
}
